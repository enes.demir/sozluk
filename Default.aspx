﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:DropDownList ID="DDL_kelimeler" runat="server">
            <asp:ListItem Value="1">Türkçe &gt;&gt; İngilizce</asp:ListItem>
            <asp:ListItem Value="2">İngilizce &gt;&gt; Türkçe</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:ListBox ID="LB_kelimeler" runat="server" Visible="False">
            <asp:ListItem>Kalem|Pencil</asp:ListItem>
            <asp:ListItem>Pencere|Window</asp:ListItem>
            <asp:ListItem>Elma|Apple</asp:ListItem>
        </asp:ListBox>
        <br />
        <br />
        <asp:Button ID="btn_ara" runat="server" onclick="btn_ara_Click" Text="Ara" />
        <asp:TextBox ID="txt_ara" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="lbl_sonuc" runat="server"></asp:Label>
    
    </div>
    </form>
</body>
</html>
