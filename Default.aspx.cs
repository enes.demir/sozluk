﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btn_ara_Click(object sender, EventArgs e)
    {
        if (DDL_kelimeler.SelectedValue == "1")
        {
            string aranankl = txt_ara.Text;
            for (int i = 0; i < LB_kelimeler.Items.Count; i++)
            {
                string metin = LB_kelimeler.Items[i].Text;
                int ayrac_no = metin.IndexOf("|");
                string tr = metin.Substring(0, ayrac_no);
                string en = metin.Substring(ayrac_no + 1);
                if (aranankl == tr)
                {
                    lbl_sonuc.Text = "İngilizcesi: " + en;
                    break;
                }
                else
                {
                    lbl_sonuc.Text = "Aradığınız kelime bulunamadı!";
                }
            }
        }
        else if (DDL_kelimeler.SelectedValue == "2")
        {
            string aranankl = txt_ara.Text;
            for (int i = 0; i < LB_kelimeler.Items.Count; i++)
            {
                string metin = LB_kelimeler.Items[i].Text;
                int ayrac_no = metin.IndexOf("|");
                string tr = metin.Substring(0, ayrac_no);
                string en = metin.Substring(ayrac_no + 1);
                if (aranankl == en)
                {
                    lbl_sonuc.Text = "Türkçesi: " + tr;
                    break;
                }
                else
                {
                    lbl_sonuc.Text = "Aradığınız kelime bulunamadı!";
                }
            }

        }
    }
}